import sys, os
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
os.chdir(os.path.dirname(os.path.realpath(__file__)))
sys.path.append("lib")
sys.path.append("ui")
from viewer_ui import Ui_Form
import questionloader, timer, multiplechoice, truefalse, manual

class Signals(QObject):
	closed = Signal()
signals = Signals()

def view(quiz):
	global Window, window, quizdata, multiplechoicequestions, truefalsequestions, manualquestions, score
	Window = QWidget()
	window = Ui_Form()
	window.setupUi(Window)

	quizdata = questionloader.getdata(quiz)

	Window.setWindowTitle(quizdata["QuizName"])
	window.view.setCurrentIndex(0)

	multiplechoicequestions = questionloader.loadMultipleChoices(quizdata["MultipleChoiceQuestions"])
	truefalsequestions = questionloader.loadTrueFalse(quizdata["TrueOrFalseQuestions"])
	manualquestions = questionloader.loadManual(quizdata["QandAQuestions"])

	timer.init(window)

	score = 0
	scoreSaver = QSettings("squidink7", "litterquiz")

	def updateScores():
		if len(scoreSaver.allKeys()) < 5:
			for i in range(5):
				scoreSaver.setValue(quizdata["QuizName"] + "/" + str(i + 1), 0)
		window.labelHiScore.setText("Highscores for " + quizdata["QuizName"])
		
		window.listHiScore.clear()
		for i in scoreSaver.allKeys():
			if i.startswith(quizdata["QuizName"]):
				window.listHiScore.addItem(str(i).split("/")[1] + ": " + str(scoreSaver.value(i)))
	
	updateScores()
	
	def showScore():
		window.view.setCurrentIndex(4)
		window.scorelbl.setText(str(score))
		scoreslist = []
		added = False
		for i in scoreSaver.allKeys():
			if score > int(scoreSaver.value(i)) and not added:
				scoreslist.append(score)
				added = True
			scoreslist.append(scoreSaver.value(i))
		for i in range(5):
			scoreSaver.setValue(quizdata["QuizName"] + "/" + str(i + 1), scoreslist[i])

	window.startbtn.clicked.connect(lambda: multiplechoice.runMultipleChoice(window, multiplechoicequestions))
	
	def finishQuiz(r: bool):
		global score
		score = 0
		if r:
			window.view.setCurrentIndex(0)
			updateScores()
		else:
			multiplechoice.signals.finished.disconnect()
			truefalse.signals.finished.disconnect()
			manual.signals.finished.disconnect()
			Window.close()
			signals.closed.emit()

	window.winretrybtn.clicked.connect(lambda: finishQuiz(True))
	window.loseretrybtn.clicked.connect(lambda: finishQuiz(True))
	window.closebtn.clicked.connect(lambda: finishQuiz(False))
	window.losecancelbtn.clicked.connect(lambda: finishQuiz(False))
	window.cancelbtn.clicked.connect(lambda: finishQuiz(False))
	
	multiplechoice.signals.finished.connect(lambda: truefalse.runTrueFalse(window, truefalsequestions))
	truefalse.signals.finished.connect(lambda: manual.runManual(window, manualquestions))
	manual.signals.finished.connect(showScore)

	def addScore(a: int):
		global score
		score += a
		timer.answered()

	multiplechoice.signals.answered.connect(addScore)
	truefalse.signals.answered.connect(addScore)
	manual.signals.answered.connect(addScore)

	Window.show()

	return True