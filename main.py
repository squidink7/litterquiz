import sys, os
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
os.chdir(os.path.dirname(os.path.realpath(__file__)))
sys.path.append("lib")
sys.path.append("ui")
from main_ui import Ui_Form
import viewer, editor

app = QApplication(sys.argv)

Window = QWidget()
window = Ui_Form()
window.setupUi(Window)

Window.setWindowIcon(QIcon("assets/icon.png"))

quiz = []
def showView():
	global quiz
	Window.close()
	quiz = QFileDialog.getOpenFileName(Window, "Select Quiz")
	if quiz[0] != "":
		viewer.view(quiz[0])
	else:
		Window.show()

window.viewbtn.clicked.connect(showView)
viewer.signals.closed.connect(Window.show)

def showEdit():
	Window.close()
	editor.edit()

window.editbtn.clicked.connect(showEdit)

Window.show()
sys.exit(app.exec())