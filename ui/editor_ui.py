# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'editor.ui'
##
## Created by: Qt User Interface Compiler version 6.3.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QLabel, QLineEdit,
    QPushButton, QRadioButton, QSizePolicy, QSpacerItem,
    QStackedWidget, QVBoxLayout, QWidget)
import assets_rc

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(507, 619)
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.view = QStackedWidget(Form)
        self.view.setObjectName(u"view")
        self.page_3 = QWidget()
        self.page_3.setObjectName(u"page_3")
        self.verticalLayout_3 = QVBoxLayout(self.page_3)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.lineEdit_6 = QLineEdit(self.page_3)
        self.lineEdit_6.setObjectName(u"lineEdit_6")
        self.lineEdit_6.setStyleSheet(u"font: 75 26pt;")

        self.verticalLayout_3.addWidget(self.lineEdit_6)

        self.view.addWidget(self.page_3)
        self.page = QWidget()
        self.page.setObjectName(u"page")
        self.verticalLayout_2 = QVBoxLayout(self.page)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_2 = QLabel(self.page)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMinimumSize(QSize(0, 200))
        self.label_2.setStyleSheet(u"image: url(:/Images/assets/edit.png);\n"
"font: 75 20pt;\n"
"color: rgb(93, 93, 93);")
        self.label_2.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.label_2)

        self.mcQuestion = QLineEdit(self.page)
        self.mcQuestion.setObjectName(u"mcQuestion")
        self.mcQuestion.setStyleSheet(u"font: 75 18pt;")

        self.verticalLayout_2.addWidget(self.mcQuestion)

        self.mcAnswer1 = QLineEdit(self.page)
        self.mcAnswer1.setObjectName(u"mcAnswer1")

        self.verticalLayout_2.addWidget(self.mcAnswer1)

        self.mcAnswer2 = QLineEdit(self.page)
        self.mcAnswer2.setObjectName(u"mcAnswer2")

        self.verticalLayout_2.addWidget(self.mcAnswer2)

        self.mcAnswer3 = QLineEdit(self.page)
        self.mcAnswer3.setObjectName(u"mcAnswer3")

        self.verticalLayout_2.addWidget(self.mcAnswer3)

        self.mcAnswer4 = QLineEdit(self.page)
        self.mcAnswer4.setObjectName(u"mcAnswer4")

        self.verticalLayout_2.addWidget(self.mcAnswer4)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.view.addWidget(self.page)
        self.page_2 = QWidget()
        self.page_2.setObjectName(u"page_2")
        self.verticalLayout_4 = QVBoxLayout(self.page_2)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.label_3 = QLabel(self.page_2)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMinimumSize(QSize(0, 200))
        self.label_3.setStyleSheet(u"image: url(:/Images/assets/edit.png);\n"
"font: 75 20pt;\n"
"color: rgb(93, 93, 93);")
        self.label_3.setAlignment(Qt.AlignCenter)

        self.verticalLayout_4.addWidget(self.label_3)

        self.tfQuestion = QLineEdit(self.page_2)
        self.tfQuestion.setObjectName(u"tfQuestion")
        self.tfQuestion.setStyleSheet(u"font: 75 18pt;")

        self.verticalLayout_4.addWidget(self.tfQuestion)

        self.tfTrue = QRadioButton(self.page_2)
        self.tfTrue.setObjectName(u"tfTrue")

        self.verticalLayout_4.addWidget(self.tfTrue)

        self.tfFalse = QRadioButton(self.page_2)
        self.tfFalse.setObjectName(u"tfFalse")

        self.verticalLayout_4.addWidget(self.tfFalse)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_2)

        self.view.addWidget(self.page_2)
        self.page_4 = QWidget()
        self.page_4.setObjectName(u"page_4")
        self.verticalLayout_5 = QVBoxLayout(self.page_4)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.label_4 = QLabel(self.page_4)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setMinimumSize(QSize(0, 200))
        self.label_4.setStyleSheet(u"image: url(:/Images/assets/edit.png);\n"
"font: 75 20pt;\n"
"color: rgb(93, 93, 93);")
        self.label_4.setAlignment(Qt.AlignCenter)

        self.verticalLayout_5.addWidget(self.label_4)

        self.mQuestion = QLineEdit(self.page_4)
        self.mQuestion.setObjectName(u"mQuestion")
        self.mQuestion.setStyleSheet(u"font: 75 18pt;")

        self.verticalLayout_5.addWidget(self.mQuestion)

        self.mAnswer = QLineEdit(self.page_4)
        self.mAnswer.setObjectName(u"mAnswer")

        self.verticalLayout_5.addWidget(self.mAnswer)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_5.addItem(self.verticalSpacer_3)

        self.view.addWidget(self.page_4)

        self.verticalLayout.addWidget(self.view)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.prevbtn = QPushButton(Form)
        self.prevbtn.setObjectName(u"prevbtn")

        self.horizontalLayout.addWidget(self.prevbtn)

        self.delbtn = QPushButton(Form)
        self.delbtn.setObjectName(u"delbtn")

        self.horizontalLayout.addWidget(self.delbtn)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)

        self.addbtn = QPushButton(Form)
        self.addbtn.setObjectName(u"addbtn")

        self.horizontalLayout.addWidget(self.addbtn)

        self.nextbtn = QPushButton(Form)
        self.nextbtn.setObjectName(u"nextbtn")

        self.horizontalLayout.addWidget(self.nextbtn)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.prevqbtn = QPushButton(Form)
        self.prevqbtn.setObjectName(u"prevqbtn")

        self.horizontalLayout_2.addWidget(self.prevqbtn)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.nextqbtn = QPushButton(Form)
        self.nextqbtn.setObjectName(u"nextqbtn")

        self.horizontalLayout_2.addWidget(self.nextqbtn)

        self.savebtn = QPushButton(Form)
        self.savebtn.setObjectName(u"savebtn")

        self.horizontalLayout_2.addWidget(self.savebtn)


        self.verticalLayout.addLayout(self.horizontalLayout_2)


        self.retranslateUi(Form)

        self.view.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Editor", None))
        self.lineEdit_6.setText("")
        self.lineEdit_6.setPlaceholderText(QCoreApplication.translate("Form", u"Quiz Title", None))
        self.label_2.setText("")
        self.mcQuestion.setText("")
        self.mcQuestion.setPlaceholderText(QCoreApplication.translate("Form", u"Question", None))
        self.mcAnswer1.setPlaceholderText(QCoreApplication.translate("Form", u"Correct Answer", None))
        self.mcAnswer2.setPlaceholderText(QCoreApplication.translate("Form", u"Wrong Answer 1", None))
        self.mcAnswer3.setPlaceholderText(QCoreApplication.translate("Form", u"Wrong Answer 2", None))
        self.mcAnswer4.setPlaceholderText(QCoreApplication.translate("Form", u"Wrong Answer 3", None))
        self.label_3.setText("")
        self.tfQuestion.setText("")
        self.tfQuestion.setPlaceholderText(QCoreApplication.translate("Form", u"Question", None))
        self.tfTrue.setText(QCoreApplication.translate("Form", u"True", None))
        self.tfFalse.setText(QCoreApplication.translate("Form", u"False", None))
        self.label_4.setText("")
        self.mQuestion.setText("")
        self.mQuestion.setPlaceholderText(QCoreApplication.translate("Form", u"Question", None))
#if QT_CONFIG(tooltip)
        self.mAnswer.setToolTip(QCoreApplication.translate("Form", u"CaSe InSeNsItIvE", None))
#endif // QT_CONFIG(tooltip)
        self.mAnswer.setText("")
        self.mAnswer.setPlaceholderText(QCoreApplication.translate("Form", u"Answer", None))
        self.prevbtn.setText(QCoreApplication.translate("Form", u"Previous", None))
        self.delbtn.setText(QCoreApplication.translate("Form", u"Delete Question", None))
        self.addbtn.setText(QCoreApplication.translate("Form", u"Add Question", None))
        self.nextbtn.setText(QCoreApplication.translate("Form", u"Next", None))
        self.prevqbtn.setText(QCoreApplication.translate("Form", u"Previous Page", None))
        self.nextqbtn.setText(QCoreApplication.translate("Form", u"Next Page", None))
        self.savebtn.setText(QCoreApplication.translate("Form", u"Save", None))
    # retranslateUi

