# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main.ui'
##
## Created by: Qt User Interface Compiler version 6.3.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QLabel, QPushButton,
    QSizePolicy, QVBoxLayout, QWidget)

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(400, 300)
        Form.setStyleSheet(u"background-color: rgb(34, 34, 34);\n"
"color: rgb(255, 255, 255);")
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setStyleSheet(u"font: 75 26pt;")
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.label)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.viewbtn = QPushButton(Form)
        self.viewbtn.setObjectName(u"viewbtn")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.viewbtn.sizePolicy().hasHeightForWidth())
        self.viewbtn.setSizePolicy(sizePolicy1)
        self.viewbtn.setStyleSheet(u"QWidget {\n"
"	background-color: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.5, fx:1, fy:0.5, stop:0 rgba(0, 0, 255, 255), stop:1 rgba(34, 34, 34, 255));\n"
"	border: 0;\n"
"	font: 75 20pt;\n"
"}\n"
"QWidget:hover {\n"
"	background-color: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.5, fx:1, fy:0.5, stop:0.5 rgba(0, 0, 255, 255), stop:1 rgba(34, 34, 34, 255));\n"
"	border: 0;\n"
"	font: 75 20pt;\n"
"}")
        self.viewbtn.setFlat(False)

        self.horizontalLayout.addWidget(self.viewbtn)

        self.editbtn = QPushButton(Form)
        self.editbtn.setObjectName(u"editbtn")
        sizePolicy1.setHeightForWidth(self.editbtn.sizePolicy().hasHeightForWidth())
        self.editbtn.setSizePolicy(sizePolicy1)
        self.editbtn.setStyleSheet(u"QWidget {\n"
"	background-color: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.5, fx:0, fy:0.5, stop:0 rgba(255, 0, 0, 255), stop:1 rgba(34, 34, 34, 255));\n"
"	border: 0;\n"
"	font: 75 20pt;\n"
"}\n"
"QWidget:hover {\n"
"	background-color: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.5, fx:0, fy:0.5, stop:0.5 rgba(255, 0, 0, 255), stop:1 rgba(34, 34, 34, 255));\n"
"	border: 0;\n"
"	font: 75 20pt;\n"
"}")
        self.editbtn.setFlat(False)

        self.horizontalLayout.addWidget(self.editbtn)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Main Menu", None))
        self.label.setText(QCoreApplication.translate("Form", u"Quiz App", None))
        self.viewbtn.setText(QCoreApplication.translate("Form", u"View", None))
        self.editbtn.setText(QCoreApplication.translate("Form", u"Edit", None))
    # retranslateUi

