# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'viewer.ui'
##
## Created by: Qt User Interface Compiler version 6.3.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QLabel, QLineEdit,
    QListWidget, QListWidgetItem, QProgressBar, QPushButton,
    QSizePolicy, QSpacerItem, QStackedWidget, QVBoxLayout,
    QWidget)
import assets_rc

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(382, 569)
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.view = QStackedWidget(Form)
        self.view.setObjectName(u"view")
        self.start = QWidget()
        self.start.setObjectName(u"start")
        self.verticalLayout_4 = QVBoxLayout(self.start)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_3)

        self.title = QLabel(self.start)
        self.title.setObjectName(u"title")
        self.title.setStyleSheet(u"font: 75 30pt;")
        self.title.setAlignment(Qt.AlignCenter)

        self.verticalLayout_4.addWidget(self.title)

        self.verticalSpacer_5 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_5)

        self.labelHiScore = QLabel(self.start)
        self.labelHiScore.setObjectName(u"labelHiScore")

        self.verticalLayout_4.addWidget(self.labelHiScore)

        self.listHiScore = QListWidget(self.start)
        self.listHiScore.setObjectName(u"listHiScore")
        self.listHiScore.setStyleSheet(u"font: 75 18pt;")
        self.listHiScore.setTextElideMode(Qt.ElideRight)

        self.verticalLayout_4.addWidget(self.listHiScore)

        self.verticalSpacer_6 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_6)

        self.startbtn = QPushButton(self.start)
        self.startbtn.setObjectName(u"startbtn")
        self.startbtn.setStyleSheet(u"font: 75 26pt;")

        self.verticalLayout_4.addWidget(self.startbtn)

        self.cancelbtn = QPushButton(self.start)
        self.cancelbtn.setObjectName(u"cancelbtn")

        self.verticalLayout_4.addWidget(self.cancelbtn)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_4)

        self.view.addWidget(self.start)
        self.mc = QWidget()
        self.mc.setObjectName(u"mc")
        self.verticalLayout_2 = QVBoxLayout(self.mc)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.mcHeader = QLabel(self.mc)
        self.mcHeader.setObjectName(u"mcHeader")
        self.mcHeader.setMinimumSize(QSize(0, 150))
        self.mcHeader.setBaseSize(QSize(0, 0))
        self.mcHeader.setStyleSheet(u"font: 75 20pt;")
        self.mcHeader.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.mcHeader)

        self.mcQuestion = QLabel(self.mc)
        self.mcQuestion.setObjectName(u"mcQuestion")
        self.mcQuestion.setStyleSheet(u"font: 75 14pt \"Google Sans\";")
        self.mcQuestion.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.mcQuestion)

        self.answer1 = QPushButton(self.mc)
        self.answer1.setObjectName(u"answer1")

        self.verticalLayout_2.addWidget(self.answer1)

        self.answer2 = QPushButton(self.mc)
        self.answer2.setObjectName(u"answer2")

        self.verticalLayout_2.addWidget(self.answer2)

        self.answer3 = QPushButton(self.mc)
        self.answer3.setObjectName(u"answer3")

        self.verticalLayout_2.addWidget(self.answer3)

        self.answer4 = QPushButton(self.mc)
        self.answer4.setObjectName(u"answer4")

        self.verticalLayout_2.addWidget(self.answer4)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.timerLabel = QLabel(self.mc)
        self.timerLabel.setObjectName(u"timerLabel")

        self.horizontalLayout.addWidget(self.timerLabel)

        self.mcTimerTime = QLabel(self.mc)
        self.mcTimerTime.setObjectName(u"mcTimerTime")

        self.horizontalLayout.addWidget(self.mcTimerTime)

        self.mcTimerBar = QProgressBar(self.mc)
        self.mcTimerBar.setObjectName(u"mcTimerBar")
        self.mcTimerBar.setMaximum(10)
        self.mcTimerBar.setValue(10)
        self.mcTimerBar.setTextVisible(False)

        self.horizontalLayout.addWidget(self.mcTimerBar)


        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.verticalSpacer = QSpacerItem(20, 118, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.view.addWidget(self.mc)
        self.tf = QWidget()
        self.tf.setObjectName(u"tf")
        self.verticalLayout_3 = QVBoxLayout(self.tf)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.mcHeader_2 = QLabel(self.tf)
        self.mcHeader_2.setObjectName(u"mcHeader_2")
        self.mcHeader_2.setMinimumSize(QSize(0, 150))
        self.mcHeader_2.setBaseSize(QSize(0, 0))
        self.mcHeader_2.setStyleSheet(u"font: 75 20pt;")
        self.mcHeader_2.setAlignment(Qt.AlignCenter)

        self.verticalLayout_3.addWidget(self.mcHeader_2)

        self.tfQuestion = QLabel(self.tf)
        self.tfQuestion.setObjectName(u"tfQuestion")
        self.tfQuestion.setStyleSheet(u"font: 75 14pt \"Google Sans\";")
        self.tfQuestion.setWordWrap(True)

        self.verticalLayout_3.addWidget(self.tfQuestion)

        self.truebtn = QPushButton(self.tf)
        self.truebtn.setObjectName(u"truebtn")

        self.verticalLayout_3.addWidget(self.truebtn)

        self.falsebtn = QPushButton(self.tf)
        self.falsebtn.setObjectName(u"falsebtn")

        self.verticalLayout_3.addWidget(self.falsebtn)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.timerLabel_2 = QLabel(self.tf)
        self.timerLabel_2.setObjectName(u"timerLabel_2")

        self.horizontalLayout_2.addWidget(self.timerLabel_2)

        self.tfTimerTime = QLabel(self.tf)
        self.tfTimerTime.setObjectName(u"tfTimerTime")

        self.horizontalLayout_2.addWidget(self.tfTimerTime)

        self.tfTimerBar = QProgressBar(self.tf)
        self.tfTimerBar.setObjectName(u"tfTimerBar")
        self.tfTimerBar.setMaximum(10)
        self.tfTimerBar.setValue(10)
        self.tfTimerBar.setTextVisible(False)

        self.horizontalLayout_2.addWidget(self.tfTimerBar)


        self.verticalLayout_3.addLayout(self.horizontalLayout_2)

        self.verticalSpacer_2 = QSpacerItem(20, 222, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_2)

        self.view.addWidget(self.tf)
        self.page = QWidget()
        self.page.setObjectName(u"page")
        self.verticalLayout_5 = QVBoxLayout(self.page)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.mcHeader_3 = QLabel(self.page)
        self.mcHeader_3.setObjectName(u"mcHeader_3")
        self.mcHeader_3.setMinimumSize(QSize(0, 150))
        self.mcHeader_3.setBaseSize(QSize(0, 0))
        self.mcHeader_3.setStyleSheet(u"font: 75 20pt;")
        self.mcHeader_3.setAlignment(Qt.AlignCenter)

        self.verticalLayout_5.addWidget(self.mcHeader_3)

        self.mQuestion = QLabel(self.page)
        self.mQuestion.setObjectName(u"mQuestion")
        self.mQuestion.setStyleSheet(u"font: 75 14pt \"Google Sans\";")
        self.mQuestion.setWordWrap(True)

        self.verticalLayout_5.addWidget(self.mQuestion)

        self.entry = QLineEdit(self.page)
        self.entry.setObjectName(u"entry")

        self.verticalLayout_5.addWidget(self.entry)

        self.submitbtn = QPushButton(self.page)
        self.submitbtn.setObjectName(u"submitbtn")

        self.verticalLayout_5.addWidget(self.submitbtn)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.timerLabel_3 = QLabel(self.page)
        self.timerLabel_3.setObjectName(u"timerLabel_3")

        self.horizontalLayout_3.addWidget(self.timerLabel_3)

        self.mTimerTime = QLabel(self.page)
        self.mTimerTime.setObjectName(u"mTimerTime")

        self.horizontalLayout_3.addWidget(self.mTimerTime)

        self.mTimerBar = QProgressBar(self.page)
        self.mTimerBar.setObjectName(u"mTimerBar")
        self.mTimerBar.setMaximum(20)
        self.mTimerBar.setValue(20)
        self.mTimerBar.setTextVisible(False)

        self.horizontalLayout_3.addWidget(self.mTimerBar)


        self.verticalLayout_5.addLayout(self.horizontalLayout_3)

        self.verticalSpacer_7 = QSpacerItem(20, 222, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_5.addItem(self.verticalSpacer_7)

        self.view.addWidget(self.page)
        self.page_3 = QWidget()
        self.page_3.setObjectName(u"page_3")
        self.verticalLayout_7 = QVBoxLayout(self.page_3)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalSpacer_13 = QSpacerItem(20, 93, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_7.addItem(self.verticalSpacer_13)

        self.labelWin = QLabel(self.page_3)
        self.labelWin.setObjectName(u"labelWin")
        self.labelWin.setStyleSheet(u"font: 75 30pt;\n"
"color: rgb(0, 255, 0);")
        self.labelWin.setAlignment(Qt.AlignCenter)

        self.verticalLayout_7.addWidget(self.labelWin)

        self.verticalSpacer_15 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_7.addItem(self.verticalSpacer_15)

        self.labelTimeOut_2 = QLabel(self.page_3)
        self.labelTimeOut_2.setObjectName(u"labelTimeOut_2")
        self.labelTimeOut_2.setAlignment(Qt.AlignCenter)

        self.verticalLayout_7.addWidget(self.labelTimeOut_2)

        self.scorelbl = QLabel(self.page_3)
        self.scorelbl.setObjectName(u"scorelbl")
        self.scorelbl.setStyleSheet(u"font: 75 34pt;")
        self.scorelbl.setAlignment(Qt.AlignCenter)

        self.verticalLayout_7.addWidget(self.scorelbl)

        self.verticalSpacer_12 = QSpacerItem(20, 28, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_7.addItem(self.verticalSpacer_12)

        self.closebtn = QPushButton(self.page_3)
        self.closebtn.setObjectName(u"closebtn")
        self.closebtn.setStyleSheet(u"font: 75 26pt;")

        self.verticalLayout_7.addWidget(self.closebtn)

        self.winretrybtn = QPushButton(self.page_3)
        self.winretrybtn.setObjectName(u"winretrybtn")

        self.verticalLayout_7.addWidget(self.winretrybtn)

        self.verticalSpacer_14 = QSpacerItem(20, 93, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_7.addItem(self.verticalSpacer_14)

        self.view.addWidget(self.page_3)
        self.page_2 = QWidget()
        self.page_2.setObjectName(u"page_2")
        self.verticalLayout_6 = QVBoxLayout(self.page_2)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalSpacer_9 = QSpacerItem(20, 105, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_6.addItem(self.verticalSpacer_9)

        self.labelOver = QLabel(self.page_2)
        self.labelOver.setObjectName(u"labelOver")
        self.labelOver.setStyleSheet(u"font: 75 30pt;\n"
"color: rgb(255, 0, 0);")
        self.labelOver.setAlignment(Qt.AlignCenter)

        self.verticalLayout_6.addWidget(self.labelOver)

        self.labelTimeOut = QLabel(self.page_2)
        self.labelTimeOut.setObjectName(u"labelTimeOut")
        self.labelTimeOut.setAlignment(Qt.AlignCenter)

        self.verticalLayout_6.addWidget(self.labelTimeOut)

        self.verticalSpacer_8 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_6.addItem(self.verticalSpacer_8)

        self.label = QLabel(self.page_2)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QSize(0, 100))
        self.label.setStyleSheet(u"image: url(:/Images/assets/death.png);")

        self.verticalLayout_6.addWidget(self.label)

        self.verticalSpacer_10 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_6.addItem(self.verticalSpacer_10)

        self.loseretrybtn = QPushButton(self.page_2)
        self.loseretrybtn.setObjectName(u"loseretrybtn")
        self.loseretrybtn.setStyleSheet(u"font: 75 26pt;")

        self.verticalLayout_6.addWidget(self.loseretrybtn)

        self.losecancelbtn = QPushButton(self.page_2)
        self.losecancelbtn.setObjectName(u"losecancelbtn")

        self.verticalLayout_6.addWidget(self.losecancelbtn)

        self.verticalSpacer_11 = QSpacerItem(20, 105, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_6.addItem(self.verticalSpacer_11)

        self.view.addWidget(self.page_2)

        self.verticalLayout.addWidget(self.view)


        self.retranslateUi(Form)

        self.view.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Viewer", None))
        self.title.setText(QCoreApplication.translate("Form", u"Quiz", None))
        self.labelHiScore.setText(QCoreApplication.translate("Form", u"High Scores:", None))
        self.startbtn.setText(QCoreApplication.translate("Form", u"Start", None))
        self.cancelbtn.setText(QCoreApplication.translate("Form", u"Cancel", None))
        self.mcHeader.setText(QCoreApplication.translate("Form", u"Multiple Choice Questions", None))
        self.mcQuestion.setText(QCoreApplication.translate("Form", u"Question", None))
        self.answer1.setText(QCoreApplication.translate("Form", u"Answer 1", None))
        self.answer2.setText(QCoreApplication.translate("Form", u"Answer 2", None))
        self.answer3.setText(QCoreApplication.translate("Form", u"Answer 3", None))
        self.answer4.setText(QCoreApplication.translate("Form", u"Answer 4", None))
        self.timerLabel.setText(QCoreApplication.translate("Form", u"Time Remaining:", None))
        self.mcTimerTime.setText(QCoreApplication.translate("Form", u"10s", None))
        self.mcHeader_2.setText(QCoreApplication.translate("Form", u"True False Questions", None))
        self.tfQuestion.setText(QCoreApplication.translate("Form", u"Question", None))
        self.truebtn.setText(QCoreApplication.translate("Form", u"True", None))
        self.falsebtn.setText(QCoreApplication.translate("Form", u"False", None))
        self.timerLabel_2.setText(QCoreApplication.translate("Form", u"Time Remaining:", None))
        self.tfTimerTime.setText(QCoreApplication.translate("Form", u"10s", None))
        self.mcHeader_3.setText(QCoreApplication.translate("Form", u"Typing Questions", None))
        self.mQuestion.setText(QCoreApplication.translate("Form", u"Question", None))
        self.submitbtn.setText(QCoreApplication.translate("Form", u"Submit", None))
        self.timerLabel_3.setText(QCoreApplication.translate("Form", u"Time Remaining:", None))
        self.mTimerTime.setText(QCoreApplication.translate("Form", u"20s", None))
        self.labelWin.setText(QCoreApplication.translate("Form", u"Finished!", None))
        self.labelTimeOut_2.setText(QCoreApplication.translate("Form", u"Your Score:", None))
        self.scorelbl.setText(QCoreApplication.translate("Form", u"0", None))
        self.closebtn.setText(QCoreApplication.translate("Form", u"Close", None))
        self.winretrybtn.setText(QCoreApplication.translate("Form", u"Retry", None))
        self.labelOver.setText(QCoreApplication.translate("Form", u"GAME OVER", None))
        self.labelTimeOut.setText(QCoreApplication.translate("Form", u"Time out!", None))
        self.label.setText("")
        self.loseretrybtn.setText(QCoreApplication.translate("Form", u"Retry", None))
        self.losecancelbtn.setText(QCoreApplication.translate("Form", u"Cancel", None))
    # retranslateUi

