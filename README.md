# Quiz App Framework Thing
To run the thing make sure you have `python` installed, with the `PySide6` module (you can install it by running `pip install pyside6` in a terminal/cmd).

## Screenshots:
<img src="screenshots/Screenshot_20220420_142305.png" width=300>
<img src="screenshots/Screenshot_20220420_142333.png" width=300>
<img src="screenshots/Screenshot_20220420_142423.png" width=300>
<img src="screenshots/Screenshot_20220420_142455.png" width=300>
