#!/bin/sh
/system/lib/qt6/uic -g python -o ui/main_ui.py ui-src/main.ui
/system/lib/qt6/uic -g python -o ui/viewer_ui.py ui-src/viewer.ui
/system/lib/qt6/uic -g python -o ui/editor_ui.py ui-src/editor.ui
/system/lib/qt6/rcc -g python -o ui/assets_rc.py ui-src/assets.qrc