import random
from PySide6.QtCore import QObject, Signal, QTimer
import timer

qnumber = 0

class Signals(QObject):
	answered = Signal(int)
	nextquestion = Signal()
	finished = Signal()
signals = Signals()

def runMultipleChoice(_window, _questions):
	global needsinit, updateSlider, questions, window
	questions = _questions
	window = _window
	window.answer1.clicked.connect(lambda: answerClick(window.answer1))
	window.answer2.clicked.connect(lambda: answerClick(window.answer2))
	window.answer3.clicked.connect(lambda: answerClick(window.answer3))
	window.answer4.clicked.connect(lambda: answerClick(window.answer4))

	window.view.setCurrentIndex(1)
	def updateSlider():
		window.mcTimerTime.setText(str(timer.timeremaining) + "s")
		window.mcTimerBar.setValue(timer.timeremaining)
		window.mcTimerBar.setStyleSheet("QProgressBar::Chunk{ background-color: rgb(" + str(255 - (255 * (window.mcTimerBar.value() / window.mcTimerBar.maximum()))) + ", " + str(255 * (window.mcTimerBar.value() / window.mcTimerBar.maximum())) + ", 0); }")
		window.mcTimerBar.update()
	timer.timer.timeout.connect(updateSlider)

	showQuestion()

def showQuestion():
	window.mcQuestion.setText(questions[qnumber].question)
	answers = questions[qnumber].answerlist()
	random.shuffle(answers)
	window.answer1.setText(answers[0])
	window.answer2.setText(answers[1])
	window.answer3.setText(answers[2])
	window.answer4.setText(answers[3])
	timer.timeremaining = 10
	updateSlider()
	timer.timer.start()

def answerClick(a):
	global qnumber
	score = 0
	timer.timer.stop()
	questionbtns = [window.answer1, window.answer2, window.answer3, window.answer4]
	for i in questionbtns:
		i.setEnabled(False)
	QTimer.singleShot(1000, lambda: incrementQuestion(questionbtns))
	if a.text() == questions[qnumber].correctanswer:
		a.setStyleSheet("background-color: #00ff00")
		score = timer.timeremaining
	else:
		for i in questionbtns:
			if i.text() == questions[qnumber].correctanswer:
				i.setStyleSheet("background-color: #00ff00")
			a.setStyleSheet("background-color: #ff0000")

	signals.answered.emit(score)

def incrementQuestion(questionbtns):
	global qnumber
	for i in questionbtns:
		i.setStyleSheet("")
		i.setEnabled(True)
	qnumber += 1
	if qnumber < len(questions):
		showQuestion()
	else:
		qnumber = 0
		window.answer1.clicked.disconnect()
		window.answer2.clicked.disconnect()
		window.answer3.clicked.disconnect()
		window.answer4.clicked.disconnect()
		timer.timer.timeout.disconnect(updateSlider)
		signals.finished.emit()