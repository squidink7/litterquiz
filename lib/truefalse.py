from PySide6.QtCore import QObject, Signal, QTimer
import timer

qnumber = 0

class Signals(QObject):
	answered = Signal(int)
	nextquestion = Signal()
	finished = Signal()
signals = Signals()

def runTrueFalse(_window, _questions):
	global needsinit, updateSlider, questions, window
	questions = _questions
	window = _window
	window.truebtn.clicked.connect(lambda: answerClick(window.truebtn))
	window.falsebtn.clicked.connect(lambda: answerClick(window.falsebtn))
	
	window.view.setCurrentIndex(2)
	def updateSlider():
		window.tfTimerTime.setText(str(timer.timeremaining) + "s")
		window.tfTimerBar.setValue(timer.timeremaining)
		window.tfTimerBar.setStyleSheet("QProgressBar::Chunk{ background-color: rgb(" + str(255 - (255 * (window.tfTimerBar.value() / window.tfTimerBar.maximum()))) + ", " + str(255 * (window.tfTimerBar.value() / window.tfTimerBar.maximum())) + ", 0); }")
		window.tfTimerBar.update()
	timer.timer.timeout.connect(updateSlider)
	
	showQuestion()

def showQuestion():
	window.tfQuestion.setText(questions[qnumber].question)
	timer.timeremaining = 10
	updateSlider()
	timer.timer.start()

def answerClick(a):
	global qnumber
	score = 0
	timer.timer.stop()
	questionbtns = [window.truebtn, window.falsebtn]
	for i in questionbtns:
		i.setEnabled(False)
	QTimer.singleShot(1000, lambda: incrementQuestion(questionbtns))
	if str(a.text()) == str(questions[qnumber].istrue):
		a.setStyleSheet("background-color: #00ff00")
		score = timer.timeremaining
	else:
		for i in questionbtns:
			if i.text().lower() == questions[qnumber].istrue:
				i.setStyleSheet("background-color: #00ff00")
			a.setStyleSheet("background-color: #ff0000")

	signals.answered.emit(score)

def incrementQuestion(questionbtns):
	global qnumber
	for i in questionbtns:
		i.setStyleSheet("")
		i.setEnabled(True)
	qnumber += 1
	if qnumber < len(questions):
		showQuestion()
	else:
		qnumber = 0
		window.truebtn.clicked.disconnect()
		window.falsebtn.clicked.disconnect()
		timer.timer.timeout.disconnect(updateSlider)
		signals.finished.emit()