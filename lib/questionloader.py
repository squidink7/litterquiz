from questions import *
from PySide6.QtCore import QJsonDocument

def getdata(quiz):
	with open(quiz) as file:
		rawquizdata = file.read()

	quizfile = QJsonDocument().fromJson(bytes(rawquizdata, encoding="utf-8"))
	return quizfile.object()

def loadMultipleChoices(quizdata):
	questionslist = []
	for i in range(len(quizdata)):
		questionslist.append(MultipleChoice(quizdata[i]["Question"], quizdata[i]["CorrectAnswer"], quizdata[i]["WrongAnswers"][0], quizdata[i]["WrongAnswers"][1], quizdata[i]["WrongAnswers"][2]))
	return questionslist

def loadTrueFalse(quizdata):
	questionslist = []
	for i in range(len(quizdata)):
		questionslist.append(TrueFalse(quizdata[i]["Question"], quizdata[i]["Answer"]))
	return questionslist

def loadManual(quizdata):
	questionslist = []
	for i in range(len(quizdata)):
		questionslist.append(ManualEntry(quizdata[i]["Question"], quizdata[i]["Answer"]))
	return questionslist