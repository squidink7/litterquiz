import random
from PySide6.QtCore import QObject, Signal, QTimer
import timer

qnumber = 0

class Signals(QObject):
	answered = Signal(int)
	nextquestion = Signal()
	finished = Signal()
signals = Signals()

def runManual(_window, _questions):
	global needsinit, updateSlider, questions, window
	questions = _questions
	window = _window
	window.submitbtn.clicked.connect(answerClick)

	window.view.setCurrentIndex(3)

	window.entry.returnPressed.connect(answerClick)

	def updateSlider():
		window.mTimerTime.setText(str(timer.timeremaining) + "s")
		window.mTimerBar.setValue(timer.timeremaining)
		window.mTimerBar.setStyleSheet("QProgressBar::Chunk{ background-color: rgb(" + str(255 - (255 * (window.mTimerBar.value() / window.mTimerBar.maximum()))) + ", " + str(255 * (window.mTimerBar.value() / window.mTimerBar.maximum())) + ", 0); }")
		window.mTimerBar.update()
	timer.timer.timeout.connect(updateSlider)

	showQuestion()

def showQuestion():
	window.mQuestion.setText(questions[qnumber].question)
	timer.timeremaining = 20
	updateSlider()
	timer.timer.start()

def answerClick():
	window.submitbtn.setEnabled(False)
	score = 0
	timer.timer.stop()
	QTimer.singleShot(1000, incrementQuestion)
	if window.entry.text().lower() == questions[qnumber].answer.lower():
		window.entry.setStyleSheet("background-color: #00ff00;")
		score = timer.timeremaining
	else:
		window.entry.setStyleSheet("background-color: #ff0000")
		window.entry.setText(questions[qnumber].answer)

	signals.answered.emit(score)

def incrementQuestion():
	global qnumber
	window.entry.setText("")
	window.entry.setStyleSheet("")
	window.submitbtn.setEnabled(True)
	qnumber += 1
	if qnumber < len(questions):
		showQuestion()
	else:
		qnumber = 0
		window.submitbtn.clicked.disconnect()
		window.entry.returnPressed.disconnect()
		timer.timer.timeout.disconnect(updateSlider)
		signals.finished.emit()