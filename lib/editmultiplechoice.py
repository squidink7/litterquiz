import sys
sys.path.append("lib")
import questions

currentquestion = 0
mcquestions = []

def init(_window):
	global window
	window = _window

def showQuestion(i = 0):
	global mcquestions, currentquestion
	currentquestion += i
	while len(mcquestions) <= currentquestion:
		mcquestions.append(questions.MultipleChoice())
	window.mcQuestion.setText(mcquestions[currentquestion].question)
	window.mcAnswer1.setText(mcquestions[currentquestion].correctanswer)
	window.mcAnswer2.setText(mcquestions[currentquestion].wronganswer0)
	window.mcAnswer3.setText(mcquestions[currentquestion].wronganswer1)
	window.mcAnswer4.setText(mcquestions[currentquestion].wronganswer2)

def saveQuestion(window):
	mcquestions[currentquestion].question = window.mcQuestion.text()
	mcquestions[currentquestion].correctanswer = window.mcAnswer1.text()
	mcquestions[currentquestion].wronganswer0 = window.mcAnswer2.text()
	mcquestions[currentquestion].wronganswer1 = window.mcAnswer3.text()
	mcquestions[currentquestion].wronganswer2 = window.mcAnswer4.text()