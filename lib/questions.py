class MultipleChoice:
	def __init__(self, question: str = "", correctanswer: str = "", wronganswer0: str = "", wronganswer1: str = "", wronganswer2: str = ""):
		self.question = question
		self.correctanswer = correctanswer
		self.wronganswer0 = wronganswer0
		self.wronganswer1 = wronganswer1
		self.wronganswer2 = wronganswer2

	def answerlist(self):
		return [self.correctanswer, self.wronganswer0, self.wronganswer1, self.wronganswer2]

class TrueFalse:
	def __init__(self, question: str, istrue: bool):
		self.question = question
		self.istrue = istrue

class ManualEntry:
	def __init__(self, question: str, answer: str):
		self.question = question
		self.answer = answer