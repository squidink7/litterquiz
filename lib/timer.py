from PySide6.QtCore import QTimer, QObject, Signal
from PySide6.QtMultimedia import QMediaPlayer, QAudioOutput
import assets_rc

timeremaining = 10

def init(_window):
	global drown, output, timer, window
	window = _window

	timer = QTimer()
	timer.setInterval(1000)
	timer.timeout.connect(checkAndPlaySound)

	drown = QMediaPlayer()
	output = QAudioOutput()
	drown.setSource("assets/timer.wav")
	drown.setActiveAudioTrack(0)
	drown.setAudioOutput(output)
	output.setVolume(75)

def checkAndPlaySound():
	global timeremaining
	if timeremaining > 1:
		timeremaining -= 1
	else:
		window.view.setCurrentIndex(5)
		timer.stop()
	if timeremaining == 5:
		drown.play()

def answered():
	drown.stop()
	timer.stop()
