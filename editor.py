import sys, os
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
os.chdir(os.path.dirname(os.path.realpath(__file__)))
sys.path.append("lib")
sys.path.append("ui")
from editor_ui import Ui_Form
import questions, editmultiplechoice

def edit():
	global Window, window
	Window = QWidget()
	window = Ui_Form()
	window.setupUi(Window)

	Window.setWindowTitle("Editor")
	window.view.setCurrentIndex(0)

	window.addbtn.setEnabled(False)
	window.delbtn.setEnabled(False)
	window.nextbtn.setEnabled(False)
	window.prevbtn.setEnabled(False)

	editmultiplechoice.init(window)

	def movePage(i):
		window.view.setCurrentIndex(window.view.currentIndex() + i)
		moveQuestion()
	
	window.nextqbtn.clicked.connect(lambda: movePage(1))
	window.prevqbtn.clicked.connect(lambda: movePage(-1))

	def moveQuestion(i = 0):
		if window.view.currentIndex() == 1:
			editmultiplechoice.showQuestion(i)
		elif window.view.currentIndex() == 2:
			pass

	window.nextbtn.clicked.connect(lambda: moveQuestion(1))
	window.nextbtn.clicked.connect(lambda: moveQuestion(-1))

	Window.show()

	return True